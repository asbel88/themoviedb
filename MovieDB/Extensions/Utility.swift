//  Created by ASBEL PARRA on 4/17/19.
//

import Foundation
import UIKit

extension Bundle {
    
    // Retrieves string from localized string file, Localizable.strings
    class func localizedStringForKey(_ key: String) -> String{
        return Bundle.main.localizedString(forKey: key, value: nil, table: nil)
    }
}

extension UIColor {
    
    class func mainColor () -> UIColor
    {
        return #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
    }
}

extension UIButton {
    
    func enableButton() {
        self.isEnabled = true
        self.alpha = 1
    }
    
    func disableButton() {
        self.isEnabled = false
        self.alpha = 0.7
    }
    
}

extension UITextField {
    
    func underlined(){
        self.borderStyle = UITextField.BorderStyle.none
        let border = CALayer()
        let width = CGFloat(2)
        border.backgroundColor = UIColor.mainColor().cgColor
        border.borderColor = UIColor.mainColor().cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: width)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension UIViewController {
    
    func getController (_ idViewController : String, storyboardName : String)  -> UIViewController? {
        
        var controller: UIViewController?
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        if let identifiers = (storyboard.value(forKey: "identifierToNibNameMap") as?
            [String : String])?.values {
            if Array(identifiers).contains(idViewController) {
                controller = storyboard.instantiateViewController(withIdentifier: idViewController)

            }
        }
        return controller
    }
}

extension String {
    
    static func stringFromNumber(_ number : NSNumber, useDecimalStyle : Bool = false) -> String? {
        let numberFormatter : NumberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = useDecimalStyle ? .decimal : .none
        numberFormatter.groupingSeparator = ((Locale.current as NSLocale).object(forKey: NSLocale.Key.groupingSeparator) as! String)
        numberFormatter.groupingSize = 3
        return numberFormatter.string(from: number)
    }
    
}

extension Date {
    
    @nonobjc static let calendar : Calendar = Date.getUTCCalendar()
    
    fileprivate static func getUTCCalendar() -> Calendar {
        var calendar : Calendar = Calendar.autoupdatingCurrent
        calendar.timeZone = TimeZone(identifier: "UTC")!
        return calendar
    }
    
    static func getDateFormatter(_ format : String) -> DateFormatter {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = calendar.timeZone
        return dateFormatter
    }
    
    static func dateForString(_ dateString : String) -> Date? {
        let dateFormatter : DateFormatter = getDateFormatter("YYYY-MM-DD")
        return dateFormatter.date(from: dateString)
    }
    
    static func getMMM_dd_yyyyDateFormatter() -> DateFormatter {
        return getDateFormatter("MMM dd, yyyy")
    }
}
