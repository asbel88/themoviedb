//  Created by ASBEL PARRA on 4/17/19.
//

import UIKit

extension UIFont {
    
    fileprivate enum FontSize : CGFloat {
        case bigger = 40.0
        case big = 29.0
        case header = 20.0
        case bodyTitle = 18.0
        case body = 16.0
        case bodySmall = 15.0
        case small = 14.0
        case extraSmall = 12.0
    }
    
    class func defaultFont(_ size : CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    
    class func defaultFontBold(_ size : CGFloat) -> UIFont {
        return UIFont.boldSystemFont(ofSize: size)
    }
    
    fileprivate func withTraits(_ traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    // TODO: add methods for every size we are gonna use on the app. DON'T FORGET
    class func bigger() -> UIFont {
        return defaultFont(FontSize.bigger.rawValue)
    }
    
    class func big() -> UIFont {
        return defaultFont(FontSize.big.rawValue)
    }
    
    class func body() -> UIFont {
        return defaultFont(FontSize.body.rawValue)
    }
    
    class func bodyItalic() -> UIFont {
        return body().withTraits(.traitItalic)
    }
    
    class func bodyBold() -> UIFont {
        return defaultFontBold(FontSize.body.rawValue)
    }
    
    class func bodyTitle() -> UIFont {
        return defaultFont(FontSize.bodyTitle.rawValue)
    }
    
    class func bodyTitleItalic() -> UIFont {
        return bodyTitle().withTraits(.traitItalic)
    }
    
    class func bodyTitleBold() -> UIFont {
        return bodyTitle().withTraits(.traitBold)
    }
    
    class func bodySmall() -> UIFont {
        return defaultFont(FontSize.bodySmall.rawValue)
    }
    
    class func bodySmallItalic() -> UIFont {
        return bodySmall().withTraits(.traitItalic)
    }
    
    class func bodySmallBold() -> UIFont {
        return bodySmall().withTraits(.traitBold)
    }
    
    class func header() -> UIFont {
        return defaultFont(FontSize.header.rawValue)
    }
    
    class func headerBold() -> UIFont {
        return header().withTraits(.traitBold)
    }
    
    class func small() -> UIFont {
        return defaultFont(FontSize.small.rawValue)
    }
    
    class func smallItalic() -> UIFont {
        return small().withTraits(.traitItalic)
    }
    
    class func smallBold() -> UIFont {
        return defaultFontBold(FontSize.small.rawValue)
    }
    
    class func extraSmall() -> UIFont {
        return defaultFont(FontSize.extraSmall.rawValue)
    }
}
