//
//  MovieAPIManager.swift
//  MovieDB
//
//  Created by ASBEL PARRA on 4/16/19.
//

import Foundation

typealias MovieAPIManager = BaseAPIManager

extension BaseAPIManager {
    
    func getPopularListMovies(_ completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler) {
        
        let queryParams = ["api_key":kPublicKey]
        
        callURLService("popular", method: .Get, parameters: queryParams, success: completionHandler, failure: failureHandler)
        
    }
    
    func getTopRatedListMovies(_ completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler) {
        
        let queryParams = ["api_key":kPublicKey]
        
        callURLService("top_rated", method: .Get, parameters: queryParams, success: completionHandler, failure: failureHandler)
        
    }
}

