//  Created by ASBEL PARRA on 4/17/19.
//

import Foundation

class Configuration {

    class func getScheme() -> String {
        return "https"
    }
    class func getHost() -> String {
        return "api.themoviedb.org"
    }
    
    class func getBaseURL() -> String {
        return "/3/movie"
    }
}
