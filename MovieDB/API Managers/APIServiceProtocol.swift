//  Created by ASBEL PARRA on 4/17/19.
//
import Foundation

protocol APIServiceProtocol {
    
    func getPopularListMovies(_ completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler)
    
     func getTopRatedListMovies(_ completionHandler: @escaping APICompletionHandler, failureHandler: @escaping APIFailureHandler)
}
