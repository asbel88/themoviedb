//  Created by ASBEL PARRA on 4/17/19.
//

import UIKit

class Movie: NSObject {

    var adult : Bool!
    var backdropPath : String!
    var genreIds : [Int] = []
    var id : Int!
    var originalLanguage : String!
    var originalTitle : String!
    var overview : String!
    var popularity : String!
    var posterPath : String!
    var releaseDate : String!
    var title : String!
    var video : Bool!
    var voteAverage : String!
    var voteCount : Int!
    
    var baseImage : String = "http://image.tmdb.org/t/p/w500"
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        adult = dictionary.boolForKey("adult")
        backdropPath = dictionary.stringForKey("backdrop_path")
        id = dictionary.intForKey("id")
        originalLanguage = dictionary.stringForKey("original_language")
        originalTitle = dictionary.stringForKey("original_title")
        overview = dictionary.stringForKey("overview")
        popularity = dictionary.stringForKey("popularity")
        posterPath = baseImage + (dictionary.stringForKey("poster_path"))
        releaseDate = dictionary.stringForKey("release_date")
        title = dictionary.stringForKey("title")
        video = dictionary.boolForKey("video")
        voteAverage = String(dictionary.doubleForKey("vote_average"))
        voteCount = dictionary.intForKey("vote_count")
        
        if let genres  = dictionary["genre_ids"] as! [Int]? {
            genreIds = genres
        }
    }
}
