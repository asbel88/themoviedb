//
//  MovieCell.swift
//  MovieDB
//
//  Created by ASBEL PARRA on 4/17/19.
//  Copyright © 2019 APapps. All rights reserved.
//

import UIKit

protocol MovieCellViewDelegate: class {
    func moreButtonTapped(movie: Movie)
}

class MovieCell: UITableViewCell {
    
    @IBOutlet weak var imageMovie: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var shortDescription: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var delegate: MovieCellViewDelegate?
    var movie : Movie!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCell()
    }
    
    @IBAction func onMoreButton(_ sender: Any) {
        delegate?.moreButtonTapped(movie: self.movie)
    }
    
    func configureCell(withMovie movie : Movie) {
        self.movie = movie
        titleMovie.text = movie.title
        shortDescription.text = movie.overview
        
        let dateR = Date.dateForString(movie.releaseDate)
        let releaseDateString : String = Date.getMMM_dd_yyyyDateFormatter().string(from: dateR!)
        releaseDate.text = "Estreno: " + releaseDateString
        
        imageMovie.downloaded(from: movie.posterPath)
        
    }
    
    // MARK: - Private
    
    private func setupCell() {
        self.selectionStyle = .none
        
        titleMovie.font = UIFont.bodyBold()
        titleMovie.textColor = UIColor.darkGray
        titleMovie.textAlignment = .left
        titleMovie.numberOfLines = 1
        
        shortDescription.font = UIFont.small()
        shortDescription.textColor = UIColor.darkGray
        shortDescription.textAlignment = .left
        shortDescription.numberOfLines = 1
        
        releaseDate.font = UIFont.small()
        releaseDate.textColor = UIColor.darkGray
        releaseDate.textAlignment = .left
        releaseDate.numberOfLines = 1
        
        moreButton.setTitle( Bundle.localizedStringForKey("kMoreButton"), for: UIControl.State())
        moreButton.layer.cornerRadius = moreButton.frame.height / 2
        moreButton.backgroundColor = UIColor.white
        moreButton.setTitleColor(UIColor.mainColor(), for: UIControl.State())
        moreButton.titleLabel?.font = UIFont.bodySmallBold()
        moreButton.isHidden = false
    }
    
}
