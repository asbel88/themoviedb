//
//  MovieDetailVC.swift
//  MovieDB
//
//  Created by ASBEL PARRA on 4/16/19.
//

import UIKit

class MovieDetailVC: BaseViewController {
    
    @IBOutlet weak var imageMovie: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var largeDescription: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var vote: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    
    var movie : Movie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupView()
    }
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureView(withMovie movieSelected : Movie) {
        movie = movieSelected
        titleMovie.text = movieSelected.title
        largeDescription.text = movie.overview
        
        let dateR = Date.dateForString(movie.releaseDate)
        let releaseDateString : String = Date.getMMM_dd_yyyyDateFormatter().string(from: dateR!)
        releaseDate.text = "Estreno: " + releaseDateString
        vote.text = "Rating: " + movie.voteAverage + "/10"
        imageMovie.downloaded(from: movie.posterPath)
        
    }
    func setupView() {
        
        alertView.layer.cornerRadius = 15
        alertView.layer.masksToBounds = true
        
        imageMovie.layer.cornerRadius = 15
        imageMovie.layer.masksToBounds = true
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        
        titleMovie.font = UIFont.bodyBold()
        titleMovie.textColor = UIColor.darkGray
        titleMovie.textAlignment = .left
        titleMovie.numberOfLines = 0
        
        largeDescription.font = UIFont.small()
        largeDescription.textColor = UIColor.darkGray
        largeDescription.textAlignment = .left
        largeDescription.numberOfLines = 0
        
        releaseDate.font = UIFont.small()
        releaseDate.textColor = UIColor.darkGray
        releaseDate.textAlignment = .left
        releaseDate.numberOfLines = 1
        
        vote.font = UIFont.small()
        vote.textColor = UIColor.darkGray
        vote.textAlignment = .left
        vote.numberOfLines = 1
        
        closeButton.setTitle( "X", for: UIControl.State())
        closeButton.layer.cornerRadius = closeButton.frame.height / 2
        closeButton.backgroundColor = UIColor.mainColor()
        closeButton.setTitleColor(UIColor.white, for: UIControl.State())
        closeButton.titleLabel?.font = UIFont.bodySmallBold()
        closeButton.isHidden = false
    }
    
}
