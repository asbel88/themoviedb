//
//  ViewController.swift
//  MovieDB
//
//  Created by ASBEL PARRA on 4/16/19.
//

import UIKit

class BaseViewController: UIViewController {
    
    var scrimView : ScrimView = ScrimView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBackButton()
        self.edgesForExtendedLayout = UIRectEdge()
        self.view.backgroundColor = UIColor.mainColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrimView.frame = view.bounds
    }
    
    // MARK: ScrimView
    
    func showScrimView() {
        scrimView.presentInView(self.view)
    }
    
    func hideScrimView() {
        scrimView.dismissFromSuperview()
    }
    
    func hideScrimViewWithAnimations(_ animationBlock : @escaping () -> Void) {
        scrimView.dismissAnimated(animationBlock)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: Private methods
    
    fileprivate func setupBackButton() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func getDefaltFailureHandler(_ title : String, message : String) -> APIFailureHandler {
        return { [weak self] error in
            self?.hideScrimView()
            let alert : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Bundle.localizedStringForKey("kOkButtonTitle"), style: .cancel, handler: nil))
        }
    }
}

