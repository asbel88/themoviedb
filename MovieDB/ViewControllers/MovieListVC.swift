//
//  MovieListVC.swift
//  MovieDB
//
//  Created by ASBEL PARRA on 4/16/19.
//

import UIKit
import RSSelectionMenu

class MovieListVC: BaseViewController , UITableViewDelegate, UITableViewDataSource, MovieCellViewDelegate {
    
    var apiClient : APIServiceProtocol = MovieAPIManager.sharedInstance
    
    @IBOutlet weak var aTableView: UITableView!
    @IBOutlet weak var titleVC: UILabel!
    @IBOutlet weak var orderButton: UIButton!
    
    var moviesArray : [Movie] = []
    
    /// Order Data Array
    var orderDataArray : [String]?
    var orderSelectedArray = [String]()
    var orderSelected : String = Bundle.localizedStringForKey("kPopularButtonText")
    
    fileprivate var shouldRemoveScrimView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = UIRectEdge()
        // Do any additional setup after loading the view.
        orderDataArray = [Bundle.localizedStringForKey("kPopularButtonText"), Bundle.localizedStringForKey("kTopButtonText") ]
        setupTableView()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPopularMovies()
    }
    
    //button actions
    @IBAction func onOrderByButton(_ sender: Any) {
        
        self.showAsAlertController(style: .alert, title: Bundle.localizedStringForKey("kOrderButtonText"), action: "Cancel", height: 80)
    }
    
    // action delegate
    func moreButtonTapped(movie: Movie) {
        let detailVC : MovieDetailVC  = self.storyboard?.instantiateViewController(withIdentifier: kMovieDetailVC) as! MovieDetailVC
        //Previous setup to detail view
        
        
        detailVC.providesPresentationContextTransitionStyle = true
        detailVC.definesPresentationContext = true
        detailVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        detailVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(detailVC, animated: true, completion: nil)
        detailVC.configureView(withMovie: movie)
    }
    
    //Mark: tableview delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Add the View Completed hardcoded cell at the bottom
        let cell : MovieCell = aTableView.dequeueReusableCell(withIdentifier: "MovieCell") as! MovieCell
        cell.configureCell(withMovie: moviesArray[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    //Mark: Setup viewcontroller
    
    fileprivate func setupView() {
        let titlebutton = Bundle.localizedStringForKey("kOrderButtonText") + orderSelected
        orderButton.setTitle( titlebutton, for: UIControl.State())
        orderButton.layer.cornerRadius = orderButton.frame.height / 2
        orderButton.backgroundColor = UIColor.white
        orderButton.setTitleColor(UIColor.mainColor(), for: UIControl.State())
        orderButton.titleLabel?.font = UIFont.bodySmallBold()
        orderButton.isHidden = false
        
        self.titleVC.text = Bundle.localizedStringForKey("kMovieListVC")
        self.titleVC.font = UIFont.bodyTitleBold()
        self.titleVC.textColor = UIColor.white
    }
    
    fileprivate func setupTableView() {
        
        aTableView.alwaysBounceVertical = false
        aTableView.dataSource = self
        aTableView.delegate = self
        aTableView.backgroundColor = UIColor.clear
        aTableView.register(UINib.init(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
    }
    
    //Mark: Call Services
    
    fileprivate func getPopularMovies(){
        let success : APICompletionHandler = { [weak self] data in
            self?.hideScrimView()
            if let methods = data as? [Any] {
                self?.handleMoviesResponse(methods)
            }
        }
        
        let failure : APIFailureHandler = getDefaltFailureHandler(Bundle.localizedStringForKey("kActivitiesErrorTitle"), message: Bundle.localizedStringForKey("kActivitiesErrorMessage"))
        showScrimView()
        apiClient.getPopularListMovies(success, failureHandler: failure)
    }
    
    fileprivate func getTopRatedMovies(){
        let success : APICompletionHandler = { [weak self] data in
            self?.hideScrimView()
            if let methods = data as? [Any] {
                self?.handleMoviesResponse(methods)
            }
        }
        
        let failure : APIFailureHandler = getDefaltFailureHandler(Bundle.localizedStringForKey("kActivitiesErrorTitle"), message: Bundle.localizedStringForKey("kActivitiesErrorMessage"))
        showScrimView()
        apiClient.getTopRatedListMovies(success, failureHandler: failure)
    }
    
    fileprivate func handleMoviesResponse(_ moviesList : [Any]) {
        
        moviesArray.removeAll()
        
        for movie in moviesList {
            let movieL : Movie = Movie(fromDictionary: movie as! [String : Any])
            moviesArray.append(movieL)
        }
        
        aTableView.reloadData()
    }
    
    // needed funcions
    
    fileprivate func updateTableIfNeeded(selected: String) {
        
        if selected == orderSelected {
            return
        }
        else {
            orderSelected = selected
            switch selected {
            case Bundle.localizedStringForKey("kTopButtonText") :
                getTopRatedMovies()
            default:
                getPopularMovies()
            }
        }
    }
    
    fileprivate func showAsAlertController(style: UIAlertController.Style, title: String?, action: String?, height: Double?) {
        let selectionStyle: SelectionStyle = .single
        
        // create menu
        let selectionMenu =  RSSelectionMenu(selectionStyle: selectionStyle, dataSource: orderDataArray!) { (cell, name, indexPath) in
            cell.textLabel?.text = name
        }
        
        // provide selected items
        selectionMenu.setSelectedItems(items: orderSelectedArray) { (text, index, isSelected, selectedItems) in
        }
        
        // on dismiss handler
        selectionMenu.onDismiss = { [weak self] items in
            
            self?.orderSelectedArray = items
            
            if style == .alert {
                let titlebutton = Bundle.localizedStringForKey("kOrderButtonText") + items.first!
                self!.orderButton.setTitle( titlebutton, for: UIControl.State())
                self!.updateTableIfNeeded(selected: items.first!)
            }
        }
        
        let menuStyle: PresentationStyle = .alert(title: title, action: action, height: height)
        selectionMenu.show(style: menuStyle, from: self)
    }
    
}
